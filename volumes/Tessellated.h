/// @file Tessellated.h
/// @author mihaela.gheata@cern.ch
//
/// Includes all headers related to the tessellated volume.

#ifndef VECGEOM_VOLUMES_TESSELLATED_H_
#define VECGEOM_VOLUMES_TESSELLATED_H_

#include "base/Global.h"
#include "volumes/PlacedTessellated.h"
#include "volumes/SpecializedTessellated.h"
#include "volumes/UnplacedTessellated.h"

#endif // VECGEOM_VOLUMES_TESSELLATED_H_
