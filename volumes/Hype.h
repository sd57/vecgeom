/// @file Hype.h

#ifndef VECGEOM_VOLUMES_HYPE_H_
#define VECGEOM_VOLUMES_HYPE_H_

#include "base/Global.h"

#include "volumes/PlacedHype.h"
#include "volumes/SpecializedHype.h"
#include "volumes/UnplacedHype.h"

#endif // VECGEOM_VOLUMES_HYPE_H_
