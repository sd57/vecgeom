/// @file ParaboloidBenchmark.cpp
/// @author Marilena Bandieramonte (marilena.bandieramonte@cern.ch)

#include "volumes/LogicalVolume.h"
#include "volumes/Box.h"
#include "volumes/Paraboloid.h"
#include "benchmarking/Benchmarker.h"
#include "management/GeoManager.h"
#include "ArgParser.h"
using namespace vecgeom;

int main(int argc, char *argv[])
{
  OPTION_INT(npoints, 1024);
  OPTION_INT(nrep, 1024);
  OPTION_DOUBLE(rlo, 3.);
  OPTION_DOUBLE(rhi, 5.);
  OPTION_DOUBLE(dz, 7.);

  UnplacedBox worldUnplaced             = UnplacedBox(rhi * 4, rhi * 4, dz * 4);
  UnplacedParaboloid paraboloidUnplaced = UnplacedParaboloid(rlo, rhi, dz); // rlo=3. - rhi=5. dz=7
  LogicalVolume world("MBworld", &worldUnplaced);
  LogicalVolume paraboloid("paraboloid", &paraboloidUnplaced);
  world.PlaceDaughter(&paraboloid, &Transformation3D::kIdentity);
  VPlacedVolume *worldPlaced = world.Place();
  GeoManager::Instance().SetWorldAndClose(worldPlaced);
  std::cout << "World set\n";

  Benchmarker tester(GeoManager::Instance().GetWorld());
  tester.SetVerbosity(3);
  tester.SetPointCount(npoints);
  tester.SetRepetitions(nrep);
  return tester.RunBenchmark();
}
